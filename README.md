This is example app using [vue-parcel-template](https://gitlab.com/foxie/vue-parcel-template).

The app provides ability to search place (google autocomplete), set radius around it and select category of venue.
Result is rendered in a table component.

**You need your own google maps/places API key and foursquare API key as well.**

Project includes few workarounds, that could imply performance issues if used incorrectly.

## Getting started
Clone this repo.

Run
`yarn`

After yarn is all finnished with getting dependencies run 
`yarn dev`

This will open dev hotreload server on losalhost:1234 (if not taken).

If you want to build static site
`yarn build`

[Demo is here.](https://foursquare-example.brencic.sk/)
Note: Lacks proper responsivity and might be buggy.